﻿using System;
using System.Collections.Generic;

namespace TestTaskAlexanderBondar
{
    static class PrimeNumberSearcher // Вряд-ли нам потребуется более 1 экземпляра данного класса
    {
        public static List<int> SearchPrimeNumbers(int start, int end) // Метод для поиска простых чисел в заданном диапазоне 
        {
            var primeNumbers = new List<int>(); // Коллекция для найденых простых чисел

            if (start > end)
            {
                int temp = start;
                start = end;
                end = temp;
            }

            if (start < 1) //Дабы не выполнять кучу ненужных операций, в случае если пользователь решит поразвлечься
                start = 1;

            bool isPrimeNumber;
            bool success = false;

            for (int i = start; i <= end; i++)
            {
                isPrimeNumber = true; // Предполагаем что число простое, до тех пор пока не выясним обратное

                for (int j = 2; j < i; j++)
                {
                    if (i % j == 0)
                    {
                        isPrimeNumber = false;
                        break;
                    }
                }
                if (isPrimeNumber)
                {
                    Console.WriteLine(i + " - простое число.");
                    primeNumbers.Add(i);
                    success = true; // Если нашли в заданном диапазоне хотябы 1 простое число

                }
            }
            if (success)
            {
                Console.WriteLine("Поиск простых чисел в заданном диапазоне завершен.");
                return primeNumbers;
            }
            else
            {
                Console.WriteLine("В заданном диапазоне нет простых чисел.");
                return null;
            }
        }
    }
}
