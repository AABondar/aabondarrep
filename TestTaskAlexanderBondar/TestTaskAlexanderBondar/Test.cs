﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

namespace TestTaskAlexanderBondar
{
    [TestFixture]
    class Test
    {
        [Test]
        public void IsNullTest()
        {
            StreamReader file = null;
            int start = 0;
            int end = 0;
            string strBuffer;
            string fileName = "InParams.csv";

            try
            {
                file = new StreamReader(@"C:\Program Files\" + fileName); //Для успешной работы тестов в данном случае файл с 
                //входящими параметрами должен находится по пути C:\Program Files\)
                do
                {
                    if ((strBuffer = file.ReadLine()) != null && strBuffer.Contains("IsNullTest"))
                    {
                        int index = strBuffer.IndexOf(';');
                        string tempBuffer = strBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        start = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        end = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));

                    }
                } while (!strBuffer.Contains("IsNullTest"));
            }

            catch (IOException)
            {
                Console.WriteLine("Ошибка при попытке доступа к файлу");
                Environment.Exit(-1);
            }

            finally
            {
                if (file != null) file.Close();
            }

            if (start != 0 && end != 0)
                Assert.IsNull(PrimeNumberSearcher.SearchPrimeNumbers(start, end));
            else
                Assert.Fail();
        }

        [Test]
        public void IsNotNullTest()
        {
            StreamReader file = null;
            int start = 0;
            int end = 0;
            string strBuffer;
            string fileName = "InParams.csv";

            try
            {
                file = new StreamReader(@"C:\Program Files\" + fileName); //Для успешной работы тестов в данном случае файл с 
                //входящими параметрами должен находится по пути C:\Program Files\)
                do
                {
                    if ((strBuffer = file.ReadLine()) != null && strBuffer.Contains("IsNotNullTest"))
                    {
                        int index = strBuffer.IndexOf(';');
                        string tempBuffer = strBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        start = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        end = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));

                    }
                } while (!strBuffer.Contains("IsNotNullTest"));
            }

            catch (IOException)
            {
                Console.WriteLine("Ошибка при попытке доступа к файлу");
                Environment.Exit(-1);
            }

            finally
            {
                if (file != null) file.Close();
            }

            if (start != 0 && end != 0)
                Assert.IsNotNull(PrimeNumberSearcher.SearchPrimeNumbers(start, end));
            else
                Assert.Fail();
        }

        [Test]
        public void TypeTest()
        {
            StreamReader file = null;
            int start = 0;
            int end = 0;
            string strBuffer;
            string fileName = "InParams.csv";

            try
            {
                file = new StreamReader(@"C:\Program Files\" + fileName); //Для успешной работы тестов в данном случае файл с 
                //входящими параметрами должен находится по пути C:\Program Files\)
                do
                {
                    if ((strBuffer = file.ReadLine()) != null && strBuffer.Contains("TypeTest"))
                    {
                        int index = strBuffer.IndexOf(';');
                        string tempBuffer = strBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        start = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        end = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));

                    }
                } while (!strBuffer.Contains("TypeTest"));
            }

            catch (IOException)
            {
                Console.WriteLine("Ошибка при попытке доступа к файлу");
                Environment.Exit(-1);
            }

            finally
            {
                if (file != null) file.Close();
            }

            if (start != 0 && end != 0)
                Assert.AreEqual(typeof(List<int>), PrimeNumberSearcher.SearchPrimeNumbers(start, end).GetType());
            else
                Assert.Fail(); 
        }

        [Test]
        public void EqualityTest()
        {
            StreamReader file = null;
            int start = 0;
            int end = 0;
            string strBuffer;
            string fileName = "InParams.csv";
            var list = new List<int>();

            try
            {
                file = new StreamReader(@"C:\Program Files\" + fileName); //Для успешной работы тестов в данном случае файл с 
                //входящими параметрами должен находится по пути C:\Program Files\)
                do
                {
                    if ((strBuffer = file.ReadLine()) != null && strBuffer.Contains("EqualityTest"))
                    {
                        int index = strBuffer.IndexOf(';');
                        string tempBuffer = strBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');

                        list.Add(Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index)));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        list.Add(Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index)));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        list.Add(Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index)));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        list.Add(Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index)));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        start = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        end = Convert.ToInt32(tempBuffer);
                    }
                } while (!strBuffer.Contains("EqualityTest"));
            }

            catch (IOException)
            {
                Console.WriteLine("Ошибка при попытке доступа к файлу");
                Environment.Exit(-1);
            }

            finally
            {
                if (file != null) file.Close();
            }

            if (start != 0 && end != 0)
                Assert.AreEqual(list, PrimeNumberSearcher.SearchPrimeNumbers(start, end));
            else
                Assert.Fail();
        }

        [Test]
        public void UniqueItemsInCollectionTest()
        {
            StreamReader file = null;
            int start = 0;
            int end = 0;
            string strBuffer;
            string fileName = "InParams.csv";

            try
            {
                file = new StreamReader(@"C:\Program Files\" + fileName); //Для успешной работы тестов в данном случае файл с 
                //входящими параметрами должен находится по пути C:\Program Files\)
                do
                {
                    if ((strBuffer = file.ReadLine()) != null && strBuffer.Contains("UniqueItemsInCollectionTest"))
                    {
                        int index = strBuffer.IndexOf(';');
                        string tempBuffer = strBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        start = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        end = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));

                    }
                } while (!strBuffer.Contains("UniqueItemsInCollectionTest"));
            }

            catch (IOException)
            {
                Console.WriteLine("Ошибка при попытке доступа к файлу");
                Environment.Exit(-1);
            }

            finally
            {
                if (file != null) file.Close();
            }

            if (start != 0 && end != 0)
                CollectionAssert.AllItemsAreUnique(PrimeNumberSearcher.SearchPrimeNumbers(start, end));
            else
                Assert.Fail();
        }

        [Test]
        public void NoLessThanOneTest()
        {
            StreamReader file = null;
            int start = 0;
            int end = 0;
            int notLess = 0;
            string strBuffer;
            string fileName = "InParams.csv";

            try
            {
                file = new StreamReader(@"C:\Program Files\" + fileName); //Для успешной работы тестов в данном случае файл с 
                //входящими параметрами должен находится по пути C:\Program Files\)
                do
                {
                    if ((strBuffer = file.ReadLine()) != null && strBuffer.Contains("NoLessThanOneTest"))
                    {
                        int index = strBuffer.IndexOf(';');
                        string tempBuffer = strBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        start = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        end = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        notLess = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));
                    }
                } while (!strBuffer.Contains("NoLessThanOneTest"));
            }

            catch (IOException)
            {
                Console.WriteLine("Ошибка при попытке доступа к файлу");
                Environment.Exit(-1);
            }

            finally
            {
                if (file != null) file.Close();
            }

            if (start != 0 && end != 0)
                Assert.That(PrimeNumberSearcher.SearchPrimeNumbers(start, end), Has.None.LessThan(notLess));
            else
                Assert.Fail();
        }

        [Test]
        public void CollectionContainsTest()
        {
            StreamReader file = null;
            int start = 0;
            int end = 0;
            string strBuffer;
            string fileName = "InParams.csv";
            var list = new List<int>();

            try
            {
                file = new StreamReader(@"C:\Program Files\" + fileName); //Для успешной работы тестов в данном случае файл с 
                //входящими параметрами должен находится по пути C:\Program Files\)
                do
                {
                    if ((strBuffer = file.ReadLine()) != null && strBuffer.Contains("CollectionContainsTest"))
                    {
                        int index = strBuffer.IndexOf(';');
                        string tempBuffer = strBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');

                        list.Add(Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index)));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        list.Add(Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index)));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        list.Add(Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index)));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        list.Add(Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index)));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        index = tempBuffer.IndexOf(';');
                        start = Convert.ToInt32(tempBuffer.Remove(index, tempBuffer.Length - index));
                        index = tempBuffer.IndexOf(';');
                        tempBuffer = tempBuffer.Remove(0, index + 1);
                        end = Convert.ToInt32(tempBuffer);
                    }
                } while (!strBuffer.Contains("CollectionContainsTest"));
            }

            catch (IOException)
            {
                Console.WriteLine("Ошибка при попытке доступа к файлу");
                Environment.Exit(-1);
            }

            finally
            {
                if (file != null) file.Close();
            }

            if (start != 0 && end != 0)
                CollectionAssert.IsSubsetOf(list, PrimeNumberSearcher.SearchPrimeNumbers(start, end));
            else
                Assert.Fail();
        }
    }
}
